# Scala Test Application
This is a simple test application that demonstrates hot to use NerdVision in a Scala application.

# Building
To build this app, use sbt.

```bash
sbt assembly
```

# Running
To run this app after you have build the app run.

```bash
sbt run
```

To run the application with NerdVision then follow these steps:

1. Download NerdVision
```bash
curl -L "https://repository.sonatype.org/service/local/artifact/maven/redirect?r=central-proxy&g=com.nerdvision&a=agent&v=LATEST" --output "nerdvision.jar"
```
2. Get your NerdVision API key from [Group Settings](https://app.nerd.vision/groups/settings)
3. Run the application with NerdVision
```bash
sbt -J-javaagent:nerdvision.jar=api.key=[YOUR API KEY] run
```

# Using Docker

You can also run this app via docker, simply follow these steps:
1. Get your NerdVision API key from [Group Settings](https://app.nerd.vision/groups/settings)
2. Run the application with NerdVision
```bash
docker run -e NV_API_KEY=[YOUR API KEY] nerdvision/example:scala
```

# Need help?

- Read the full [docs](https://docs.nerd.vision) 
- Contact Support at [support@nerd.vision](mailto:support@nerd.vision)
- You can also find us on [Twitter](https://twitter.com/nerdvision) and [Facebook](https://www.facebook.com/NerdVision-366820847257558/)
- Or come and chat on [Discord](https://discord.gg/TxPG97U)
