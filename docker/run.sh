#!/bin/bash
export NV_NAME=${NV_NAME:='scala-test-app'}
export NV_REPO_URL=${NV_REPO_URL:='https://gitlab.com/nerd-vision/test-apps/java/scala'}

curl -L "https://repository.sonatype.org/service/local/artifact/maven/redirect?r=central-proxy&g=com.nerdvision&a=agent&v=LATEST" --output "/opt/service/nerdvision.jar"
java -javaagent:/opt/service/nerdvision.jar -jar /opt/service/scala-test.jar
