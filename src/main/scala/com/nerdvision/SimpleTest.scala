package com.nerdvision

import scala.collection.mutable

object SimpleTest {
  @throws[Exception]
  def checkEnd(`val`: Int, max: Int): Unit = {
    if (`val` > max) throw new Exception("Hit max executions " + `val` + " " + max)
  }
}

class SimpleTest(val testName: String) extends BaseTest {

  final private val startedAt = System.currentTimeMillis
  private var cnt = 0
  private var maxExecutions = nextMax()
  private var charCounter = mutable.Map[Character, Integer]()

  @throws[Exception]
  def message(uuid: String): Unit = {
    System.out.println(cnt + ":" + uuid)
    cnt += 1
    SimpleTest.checkEnd(cnt, maxExecutions)
    val info = makeCharCountMap(uuid)
    merge(charCounter, info)
    if ((cnt % 30) == 0) dump()
  }

  def merge(charCounter: mutable.Map[Character, Integer], newInfo: mutable.Map[Character, Integer]): Unit = {

    for ((c, i) <- newInfo) {
      val curr = charCounter.get(c)
      if (curr.isEmpty) charCounter.update(c, i)
      else charCounter.put(c, curr.get + i)
    }
  }

  def dump(): Unit = {
    System.out.println(charCounter)
    charCounter = mutable.Map[Character, Integer]()
  }

  def reset(): Unit = {
    cnt = 0
    maxExecutions = nextMax()
  }

  override def toString: String = getClass.getName + ":" + testName + ":" + startedAt + "#" + System.identityHashCode(this)
}
