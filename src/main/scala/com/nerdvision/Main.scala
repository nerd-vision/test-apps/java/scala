package com.nerdvision

import com.nerdvision.api.NerdVisionAPI

object Main {
  def main(args: Array[String]): Unit = {
    val ts = new SimpleTest("This is a test")

    while ( {
      true
    }) {
      try ts.message(ts.newId)
      catch {
        case e: Exception =>
          NerdVisionAPI.getInstance().captureException(e)
          e.printStackTrace()
          ts.reset()
      }
      Thread.sleep(100)
    }
  }
}
