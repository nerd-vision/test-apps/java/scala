package com.nerdvision

import java.util.UUID
import java.util.concurrent.ThreadLocalRandom
import scala.collection.mutable

class BaseTest {
  final protected val systemProps = System.getProperties

  def newId: String = UUID.randomUUID.toString

  def makeCharCountMap(str: String): mutable.Map[Character, Integer] = {
    val res = collection.mutable.Map[Character, Integer]()
    for (i <- 0 until str.length) {
      val c = str.charAt(i)
      val cnt = res.get(c)
      if (cnt.isEmpty) res.update(c, 0)
      else res.update(c, cnt.get + 1)
    }
    res
  }

  def nextMax(): Int = ThreadLocalRandom.current.nextInt(1, 100)
}
