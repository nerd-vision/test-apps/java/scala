lazy val root = (project in file(".")).
  settings(
    name := "scala-test-app",
    version := "1.0.0",
    scalaVersion := "2.12.8",
    mainClass in Compile := Some("com.nerdvision.Main")
  )

val nvVersion = "3.0.0-rc1"

libraryDependencies ++= Seq(
  "com.nerdvision" % "api" % nvVersion % "compile",
  "com.nerdvision" % "agent" % nvVersion % "provided",
)

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}
